#use wml::db.d.o title="Direct LDAP Access"
#use wml::vbar

<dsatoc/>

<p>A restricted subset of information is available to non-developers via LDAP.
If you wish relatively unfettered access to the LDAP database, connect to it
from a .debian.org machine, such as people.debian.org or master.debian.org.
</p>

<p>
The LDAP utilities package (<a href="https://packages.debian.org/ldap-utils">\
ldap-utils</a>) provides an utility called ldapsearch that can be used
to execute direct queries to the database. This is done by supplying
the following arguments to ldapsearch: <strong>-x -H
ldaps://db.debian.org -b dc=debian,dc=org</strong>. Alternatively, the
<strong>-H</strong> and <strong>-b</strong> options can be put in
one's ~/.ldaprc, in the following form:
</p><pre>
[ dbharris@people: ~/ ]$ cat ~/.ldaprc
URI ldaps://db.debian.org
BASE dc=debian,dc=org
</pre>

<p>
<strong>-x</strong> tells ldapsearch to use "simple" (non-SASL, non-Kerberos)
authentication. There appears to be no ~/.ldaprc option which does the same as
<strong>-x</strong>. With these parameters specified, we're ready to begin
searching. Here's an example:
</p><pre>
[ dbharris@people: ~/ ]$ ldapsearch -x uid=dbharris keyfingerprint
&lt;snip&gt;
# dbharris, users, debian, org
dn: uid=dbharris,ou=users,dc=debian,dc=org
keyFingerPrint: CC53F12435C07BC258FE7A3C157DDFD959DDCB9F
&lt;snip&gt;
</pre>

<p>
The first non-option argument (<strong>uid=dbharris</strong> in this case) is
the query to perform, and the rest of the arguments are the attributes to
return. If you only specify the query, but don't provide any attributes to
return, all readable attributes are returned. While the example was quite
simple, complex queries can be performed as well:
</p><pre>
[ dbharris@people: ~/ ]$ ldapsearch -x -H ldaps://db.debian.org -b dc=debian,dc=org '(&amp;(!(loginshell=/bin/bash))(uid=*))' loginshell
</pre>

<p>
That query shows users that do not use bash as their shell. Some other
interesting queries are:</p>
<ul>
<li>Count the number of developers
<tt>(&amp;(keyfingerprint=*)(supplementaryGid=Debian))</tt></li>
<li>Show people in a certain group <tt>gidmembership=adm</tt></li>
<li>People named james <tt>cn=james</tt></li>
<li>Someone whose last name phonetically sounds like 'Ackerma'
<tt>sn~=ackerm</tt></li>
<li>All the sparcs <tt>host=sparc</tt></li>
</ul>

<p><a href="http://www.faqs.org/rfcs/rfc2254.html">RFC 2254</a>
has more information about the filter expressions.</p>


<h1>Other LDAP Browsers</h1>
<p>
The GQ package has a graphical LDAP browser that can browse the debian.org
tree. It is somewhat ungainly with the large number of entries in our
directory, but it does work nonetheless. Configuration is similar, use the
preferences dialog to add a new host with the information given above.
<p>
Netscape has a browser for their mailer, but I have never been able to get
it to work, please email if you have any luck.
<p>
To my knowledge there are no interfaces for popular mailers like mutt and
gnus. Such an interface would allow using the directory as an enhanced address
book.
